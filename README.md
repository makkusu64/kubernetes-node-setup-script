## Kubernetes Node Setup Script

This script will install and configure a Kubernetes node on Ubuntu 20.04 with Kubernetes Version 1.24.3. It is intended to be run on a fresh install of Ubuntu 20.04. It will install and configure the following:

* Install and Configure containerd, kubeadm, kubelet and kubectl
* Network Configuration
* Dependent Packages and Configuration
* Setup kubectl auto-completion

### Usage

    sudo sh kube-node-setup.sh

### Notes

#### Version 0.1

* Initial Release
* Ubuntu 20.04 Support